/*
 * luxor-rectangle-private.h: A rectangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_RECTANGLE_H
#define LUXOR_RECTANGLE_H

/**
 * luxor_rectangle_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_rectangle {
  // TODO
} luxor_rectangle_t;

#endif /* LUXOR_RECTANGLE_H */
