/*
 * luxor-device-private.h: A device
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_DEVICE_H
#define LUXOR_DEVICE_H

/**
 * luxor_device_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_device {
  // TODO
} luxor_device_t;

#endif /* LUXOR_DEVICE_H */
