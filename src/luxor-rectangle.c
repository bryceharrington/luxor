/*
 * luxor-rectangle.c: A rectangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-rectangle
 * @title: 
 * @short_description:  type
 *
 * #luxor_rectangle_t is a type representing...
 */

#include "luxor-rectangle-private.h"

/**
 * luxor_rectangle_create:
 *
 * Allocates a new #luxor_rectangle_t.
 *
 * The contents of the returned rectangle are undefined.
 *
 * @returns The newly allocated rectangle, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_rectangle_t *
luxor_rectangle_create(void) {
  struct luxor_rectangle_t *rectangle;

  rectangle = calloc(1, sizeof (*rectangle));
  if (rectangle == NULL)
    return NULL;

  return rectangle;
}

/**
 * luxor_rectangle_init:
 * @rectangle: A #luxor_rectangle_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_rectangle_init(luxor_rectangle_t *rectangle) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_rectangle_destroy:
 * @rectangle: A #luxor_rectangle_t to be freed.
 *
 * Frees the resources allocated by luxor_rectangle_create().
 *
 * Since: 0.0
 */
void
luxor_rectangle_destroy(luxor_rectangle_t *rectangle) {
  free(rectangle);
}

