/*
 * luxor-effect.h: A effect
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_EFFECT_H
#define LUXOR_EFFECT_H

luxor_effect_t *
luxor_effect_create();

void
luxor_effect_init(luxor_effect_t *effect);

void
luxor_effect_destroy(luxor_effect_t *effect);

#endif /* LUXOR_EFFECT_H */
