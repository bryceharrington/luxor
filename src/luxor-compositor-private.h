/*
 * luxor-compositor-private.h: A compositor
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_COMPOSITOR_H
#define LUXOR_COMPOSITOR_H

/**
 * luxor_compositor_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_compositor {
  // TODO
} luxor_compositor_t;

#endif /* LUXOR_COMPOSITOR_H */
