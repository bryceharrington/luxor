/*
 * luxor-surface.h: A surface
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_SURFACE_H
#define LUXOR_SURFACE_H

luxor_surface_t *
luxor_surface_create();

void
luxor_surface_init(luxor_surface_t *surface);

void
luxor_surface_destroy(luxor_surface_t *surface);

#endif /* LUXOR_SURFACE_H */
