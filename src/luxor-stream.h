/*
 * luxor-stream.h: A stream
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_STREAM_H
#define LUXOR_STREAM_H

luxor_stream_t *
luxor_stream_create();

void
luxor_stream_init(luxor_stream_t *stream);

void
luxor_stream_destroy(luxor_stream_t *stream);

#endif /* LUXOR_STREAM_H */
