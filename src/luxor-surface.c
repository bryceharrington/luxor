/*
 * luxor-surface.c: A surface
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-surface
 * @title: 
 * @short_description:  type
 *
 * #luxor_surface_t is a type representing...
 */

#include "luxor-surface-private.h"

/**
 * luxor_surface_create:
 *
 * Allocates a new #luxor_surface_t.
 *
 * The contents of the returned surface are undefined.
 *
 * @returns The newly allocated surface, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_surface_t *
luxor_surface_create(void) {
  struct luxor_surface_t *surface;

  surface = calloc(1, sizeof (*surface));
  if (surface == NULL)
    return NULL;

  return surface;
}

/**
 * luxor_surface_init:
 * @surface: A #luxor_surface_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_surface_init(luxor_surface_t *surface) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_surface_destroy:
 * @surface: A #luxor_surface_t to be freed.
 *
 * Frees the resources allocated by luxor_surface_create().
 *
 * Since: 0.0
 */
void
luxor_surface_destroy(luxor_surface_t *surface) {
  free(surface);
}

