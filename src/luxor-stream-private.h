/*
 * luxor-stream-private.h: A stream
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_STREAM_H
#define LUXOR_STREAM_H

/**
 * luxor_stream_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_stream {
  // TODO
} luxor_stream_t;

#endif /* LUXOR_STREAM_H */
