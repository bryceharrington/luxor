/*
 * luxor-compositor.c: A compositor
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-compositor
 * @title: 
 * @short_description:  type
 *
 * #luxor_compositor_t is a type representing...
 */

#include "luxor-compositor-private.h"

/**
 * luxor_compositor_create:
 *
 * Allocates a new #luxor_compositor_t.
 *
 * The contents of the returned compositor are undefined.
 *
 * @returns The newly allocated compositor, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_compositor_t *
luxor_compositor_create(void) {
  struct luxor_compositor_t *compositor;

  compositor = calloc(1, sizeof (*compositor));
  if (compositor == NULL)
    return NULL;

  return compositor;
}

/**
 * luxor_compositor_init:
 * @compositor: A #luxor_compositor_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_compositor_init(luxor_compositor_t *compositor) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_compositor_destroy:
 * @compositor: A #luxor_compositor_t to be freed.
 *
 * Frees the resources allocated by luxor_compositor_create().
 *
 * Since: 0.0
 */
void
luxor_compositor_destroy(luxor_compositor_t *compositor) {
  free(compositor);
}

