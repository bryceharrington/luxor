/*
 * luxor-rectangle.h: A rectangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_RECTANGLE_H
#define LUXOR_RECTANGLE_H

luxor_rectangle_t *
luxor_rectangle_create();

void
luxor_rectangle_init(luxor_rectangle_t *rectangle);

void
luxor_rectangle_destroy(luxor_rectangle_t *rectangle);

#endif /* LUXOR_RECTANGLE_H */
