/*
 * luxor-font.h: A font
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_FONT_H
#define LUXOR_FONT_H

luxor_font_t *
luxor_font_create();

void
luxor_font_init(luxor_font_t *font);

void
luxor_font_destroy(luxor_font_t *font);

#endif /* LUXOR_FONT_H */
