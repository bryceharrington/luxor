/*
 * luxor-compositor.h: A compositor
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_COMPOSITOR_H
#define LUXOR_COMPOSITOR_H

luxor_compositor_t *
luxor_compositor_create();

void
luxor_compositor_init(luxor_compositor_t *compositor);

void
luxor_compositor_destroy(luxor_compositor_t *compositor);

#endif /* LUXOR_COMPOSITOR_H */
