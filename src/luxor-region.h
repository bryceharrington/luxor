/*
 * luxor-region.h: A region
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_REGION_H
#define LUXOR_REGION_H

luxor_region_t *
luxor_region_create();

void
luxor_region_init(luxor_region_t *region);

void
luxor_region_destroy(luxor_region_t *region);

#endif /* LUXOR_REGION_H */
