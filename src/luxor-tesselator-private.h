/*
 * luxor-tesselator-private.h: A tesselator
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_TESSELATOR_H
#define LUXOR_TESSELATOR_H

/**
 * luxor_tesselator_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_tesselator {
  // TODO
} luxor_tesselator_t;

#endif /* LUXOR_TESSELATOR_H */
